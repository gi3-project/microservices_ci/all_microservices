package tn.enis.publicationservice.service;


import tn.enis.publicationservice.entity.Publication;

import java.util.List;

public interface IPublicationService {
    //Crud sur les publications
    Publication addPublication(Publication m);

    void deletePublication(Long id);

    Publication updatePublication(Publication m);

    Publication findPublication(Long id);

    List<Publication> findAll();
}

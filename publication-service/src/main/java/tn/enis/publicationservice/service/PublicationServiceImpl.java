package tn.enis.publicationservice.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tn.enis.publicationservice.dao.PublicationRepository;
import tn.enis.publicationservice.entity.Publication;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class PublicationServiceImpl implements IPublicationService {

    PublicationRepository publicationRepository;

    @Override
    public Publication addPublication(Publication publication) {

        publicationRepository.save(publication);

        log.info("Publication added successfully.");
        return publication;
    }

    @Override
    public void deletePublication(Long id) {
        publicationRepository.deleteById(id);
        log.info("Publication deleted successfully.");
    }

    @Override
    public Publication updatePublication(Publication o)
    {
        Publication publication = publicationRepository.saveAndFlush(o);
        log.info("Publication updated successfully.");

        return publication;
    }

    @Override
    public Publication findPublication(Long id) {
        Publication publication = publicationRepository.findById(id).get();

        log.info("Publication founded by id={} successfully.",id);
        return publication;
    }

    @Override
    public List<Publication> findAll() {
        List<Publication> publications = publicationRepository.findAll();

        log.info("Find all publications successfully.");
        return publications;
    }

}

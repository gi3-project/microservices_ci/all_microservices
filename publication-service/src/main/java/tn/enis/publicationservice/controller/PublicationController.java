package tn.enis.publicationservice.controller;

import java.util.List;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.publicationservice.entity.Publication;
import tn.enis.publicationservice.service.IPublicationService;

@AllArgsConstructor
@RestController
@RequestMapping("/publications")
public class PublicationController {
    IPublicationService publicationService;

    @GetMapping
    public List<Publication> findPublications() {
        return publicationService.findAll();
    }

    @GetMapping("/{id}")
    public Publication findOnePublicationById(@PathVariable Long id) {
        return publicationService.findPublication(id);
    }

    @PostMapping
    public Publication addPublication(@RequestBody Publication publication) {
        return publicationService.addPublication(publication);
    }

    @DeleteMapping("/{id}")
    public void deletePublication(@PathVariable Long id) {
        publicationService.deletePublication(id);
    }

    @PutMapping("/{id}")
    public Publication updatePublication(@PathVariable Long id, @RequestBody Publication publication) {
        publication.setId(id);
        return publicationService.updatePublication(publication);
    }
}

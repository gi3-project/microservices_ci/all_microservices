package tn.enis.publicationservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.enis.publicationservice.entity.Publication;


public interface PublicationRepository extends JpaRepository<Publication,Long> {}

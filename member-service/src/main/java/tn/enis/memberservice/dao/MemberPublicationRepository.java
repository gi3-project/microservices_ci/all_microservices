package tn.enis.memberservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.enis.memberservice.entity.Member;
import tn.enis.memberservice.entity.Member_Publication;
import tn.enis.memberservice.entity.Member_Publication_Id;

import java.util.List;

public interface MemberPublicationRepository  extends JpaRepository<Member_Publication, Member_Publication_Id> {
    List<Member_Publication> findByAuteur(Member auteur);
}
package tn.enis.memberservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tn.enis.memberservice.entity.Member;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member,Long> {
    Member findByCin(String cin);
    List<Member> findByNomStartingWith(String caractere);
    List<Member> findByNom(String nom);
    @Query("from EnseignantChercheur")
    List<Member> findEnseignant();
    Member findByEmail(String email);

    Member findByNomAndPrenom(String nom, String prenom);
}

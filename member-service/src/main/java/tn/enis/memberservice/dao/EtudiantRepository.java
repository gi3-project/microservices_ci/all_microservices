package tn.enis.memberservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.enis.memberservice.entity.EnseignantChercheur;
import tn.enis.memberservice.entity.Etudiant;

import java.util.List;

public interface EtudiantRepository extends JpaRepository<Etudiant,Long> {
    List<Etudiant> findByDiplome(String diplome);
    List<Etudiant> findByDiplomeOrderByDateInscriptionDesc(String diplome);
    List<Etudiant> findByEncadrant(EnseignantChercheur enseignantChercheur);
}

package tn.enis.memberservice.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Member_Publication implements Serializable {
    @EmbeddedId
    private Member_Publication_Id id;

    @ManyToOne
    @MapsId("auteur_id")
    private Member auteur;
}
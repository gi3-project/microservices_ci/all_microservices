package tn.enis.memberservice.entity;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.*;

import java.util.Date;

@Entity
@DiscriminatorValue("Enseignant")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EnseignantChercheur extends Member {

    @Builder
    public EnseignantChercheur(Long  id,String cin, String nom, String prenom, Date dateNaissance, String photo,
                    String cv, String email, String password,String grade, String etablissement) {
        super(id, cin, nom, prenom, dateNaissance,photo, cv, email, password,null);
        this.grade = grade;
        this.etablissement = etablissement;
    }

    private String grade;
    private String etablissement;
}
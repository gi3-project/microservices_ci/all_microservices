package tn.enis.memberservice.entity;

import jakarta.persistence.*;
import lombok.*;
import tn.enis.memberservice.beans.PublicationBean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_mbr", discriminatorType =
        DiscriminatorType.STRING, length = 10)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Member implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String cin;
    private String nom;
    private String prenom;
    @Temporal(TemporalType.DATE)
    private Date dateNaissance;
    @Lob
    private String photo;
    private String cv;
    private String email;
    private String password;
    @Transient
    Collection<PublicationBean> pubs;

    @Transient
    public String getType() {
        return this.getClass().getAnnotation(DiscriminatorValue.class).value();
    }
}
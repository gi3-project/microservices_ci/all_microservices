package tn.enis.memberservice.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@DiscriminatorValue("Etudiant")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Etudiant extends Member {

    @Builder
    public Etudiant( Long  id ,String cin, String nom, String prenom, Date dateNaissance,String photo,
                     String cv, String email, String password, Date dateInscription,
                     String sujet, String diplome, EnseignantChercheur encadrant) {
        super( id, cin, nom, prenom, dateNaissance,photo, cv, email, password,null);
        this.dateInscription = dateInscription;
        this.sujet = sujet;
        this.diplome = diplome;
        this.encadrant = encadrant;
    }

    @Temporal(TemporalType.DATE)
    private Date dateInscription;
    private String diplome;
    @ManyToOne
    private EnseignantChercheur encadrant;
    private String sujet;
}

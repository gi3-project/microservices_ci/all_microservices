package tn.enis.memberservice.controller;

import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tn.enis.memberservice.entity.EnseignantChercheur;
import tn.enis.memberservice.entity.Etudiant;
import tn.enis.memberservice.entity.Member;
import tn.enis.memberservice.service.IMemberService;

@AllArgsConstructor
@RestController
@RequestMapping("/members")
public class MemberController {
    IMemberService memberService;
    @GetMapping("/fullmember/{id}")
    public Member findAFullMember(@PathVariable(name="id") Long id)
    {
        Member mbr=memberService.findMember(id);
        mbr.setPubs(memberService.findPublicationparauteur(id));
        return mbr;
    }
    @GetMapping
    public List<Member> findMembers() {
        return memberService.findAll();
    }

    @GetMapping("/enseignants")
    public List<Member> findEnseignant() {
        return memberService.findAllEnseignant();
    }

    @GetMapping("/{id}")
    public Member findOneMemberById(@PathVariable Long id) {
        return memberService.findMember(id);
    }

    @GetMapping("/search/cin")
    public Member findOneMemberByCin(@RequestParam String cin) {
        return memberService.findByCin(cin);
    }

    @GetMapping("/search/email")
    public Member findOneMemberByEmail(@RequestParam String email) {
        return memberService.findByEmail(email);
    }

    @GetMapping("/search/nom")
    public List<Member> findMembersByNom(@RequestParam String nom) {
        return memberService.findByNom(nom);
    }

    @GetMapping("/search/diplome")
    public List<Etudiant> findMembersByDiplome(@RequestParam String diplome) {
        return memberService.findByDiplome(diplome);
    }

    @PostMapping("/enseignant")
    public Member addMember(@RequestBody EnseignantChercheur m) {
        return memberService.addMember(m);
    }

    @PostMapping("/etudiant")
    public Member addMember(@RequestBody Etudiant e) {
        return memberService.addMember(e);
    }

    @DeleteMapping("/{id}")
    public void deleteMember(@PathVariable Long id) {
        memberService.deleteMember(id);
    }

    @PutMapping("/etudiant/{id}")
    public Member updatemembre(@PathVariable Long id, @RequestBody Etudiant p) {
        p.setId(id);
        return memberService.updateMember(p);
    }

    @PutMapping("/enseignant/{id}")
    public Member updateMember(@PathVariable Long id, @RequestBody EnseignantChercheur p) {
        p.setId(id);
        return memberService.updateMember(p);
    }
}

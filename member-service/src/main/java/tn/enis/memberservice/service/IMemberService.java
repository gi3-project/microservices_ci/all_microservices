package tn.enis.memberservice.service;

import tn.enis.memberservice.beans.PublicationBean;
import tn.enis.memberservice.entity.EnseignantChercheur;
import tn.enis.memberservice.entity.Etudiant;
import tn.enis.memberservice.entity.Member;

import java.util.List;

public interface IMemberService {

    List<PublicationBean> findPublicationparauteur(Long idauteur);

    void affecterauteurTopublication(Long idauteur, Long idpub);

    //Crud sur les membres
    Member addMember(Member m);

    void deleteMember(Long id);

    Member updateMember(Member m);

    Member findMember(Long id);

    List<Member> findAll();

    List<Member> findAllEnseignant();

    //Filtrage par propriété
    Member findByCin(String cin);

    Member findByEmail(String email);

    List<Member> findByNom(String nom);

    //recherche spécifique des étudiants
    List<Etudiant> findByDiplome(String diplome);

    //recherche spécifique des enseignants
    List<EnseignantChercheur> findByGrade(String grade);

    List<EnseignantChercheur> findByEtablissement(String etablissement);

    void affecterEtudiantToEnsignant(Long idEtd, Long idEns);

    List<Etudiant> findEtudiantsByEnseignant(EnseignantChercheur ens);
}

package tn.enis.memberservice.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tn.enis.memberservice.beans.PublicationBean;
import tn.enis.memberservice.dao.EnseignantChercheurRepository;
import tn.enis.memberservice.dao.EtudiantRepository;
import tn.enis.memberservice.dao.MemberPublicationRepository;
import tn.enis.memberservice.dao.MemberRepository;
import tn.enis.memberservice.entity.*;
import tn.enis.memberservice.proxies.PublicationProxyService;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class MemberServiceImpl implements IMemberService {

    MemberRepository memberRepository;
    EtudiantRepository etudiantRepository;
    EnseignantChercheurRepository enseignantChercheurRepository;
    MemberPublicationRepository memberPublicationRepository;
    PublicationProxyService publicationProxyService;

    @Override
    public List<PublicationBean> findPublicationparauteur(Long idauteur) {
        List<PublicationBean> pubs = new ArrayList<PublicationBean>();
        Member auteur = memberRepository.findById(idauteur).get();
        List<Member_Publication>
                idpubs = memberPublicationRepository.findByAuteur(auteur);
        idpubs.forEach(s -> {
                    System.out.println(s);
                    pubs.add(publicationProxyService.findPublicationById(s.getId().getPublication_id()));
                }
        );
        return pubs;
    }

    @Override
    public void affecterauteurTopublication(Long idauteur, Long idpub) {
        Member mbr = memberRepository.findById(idauteur).get();
        Member_Publication mbs = new Member_Publication();
        mbs.setAuteur(mbr);
        mbs.setId(new Member_Publication_Id(idpub, idauteur));
        memberPublicationRepository.save(mbs);
    }

    @Override
    public Member addMember(Member m) {
        if (m instanceof Etudiant) {
            Long idEns = ((Etudiant) m).getEncadrant().getId();
            ((Etudiant) m).setEncadrant(null);
            memberRepository.save(m);
            affecterEtudiantToEnsignant(m.getId(), idEns);
        } else {
            memberRepository.save(m);
        }

        log.info("Member added successfully.");
        return m;
    }

    @Override
    public void deleteMember(Long id) {
        memberRepository.deleteById(id);
        log.info("Member deleted successfully.");
    }

    @Override
    public Member updateMember(Member m) {
        Member member = memberRepository.saveAndFlush(m);
        log.info("Member updated successfully.");

        return member;

    }

    @Override
    public Member findMember(Long id) {
        Member m = memberRepository.findById(id).get();

        log.info("Member founded by id={} successfully.", id);
        return m;
    }

    @Override
    public List<Member> findAll() {
        List<Member> members = memberRepository.findAll();

        log.info("Find all members successfully.");
        return members;
    }

    @Override
    public List<Member> findAllEnseignant() {
        List<Member> members = memberRepository.findEnseignant();

        log.info("Find all teachers successfully.");
        return members;
    }

    @Override
    public Member findByCin(String cin) {
        Member member = memberRepository.findByCin(cin);

        log.info("Find member by cin successfully.");
        return member;
    }

    @Override
    public Member findByEmail(String email) {
        Member member = memberRepository.findByEmail(email);

        log.info("Find member by email successfully.");
        return member;
    }

    @Override
    public List<Member> findByNom(String nom) {
        List<Member> members = memberRepository.findByNom(nom);

        log.info("Find members by nom successfully.");
        return members;
    }

    @Override
    public List<Etudiant> findByDiplome(String diplome) {
        List<Etudiant> etudiants = etudiantRepository.findByDiplome(diplome);

        log.info("Find etudiants by diploma successfully.");
        return etudiants;
    }

    @Override
    public List<EnseignantChercheur> findByGrade(String grade) {
        List<EnseignantChercheur> enseignantChercheurs = enseignantChercheurRepository.findByGrade(grade);

        log.info("Find enseignant chercheur by grade successfully.");
        return enseignantChercheurs;
    }

    @Override
    public List<EnseignantChercheur> findByEtablissement(String etablissement) {
        List<EnseignantChercheur> enseignantChercheurs = enseignantChercheurRepository.findByEtablissement(etablissement);

        log.info("Find enseignant chercheur by etablissement successfully.");
        return enseignantChercheurs;
    }

    @Override
    public void affecterEtudiantToEnsignant(Long idEtd, Long idEns) {
        Etudiant etd = (Etudiant) memberRepository.findById(idEtd).get();
        EnseignantChercheur ens = (EnseignantChercheur) memberRepository.findById(idEns).get();
        etd.setEncadrant(ens);
        etudiantRepository.save(etd);

        log.info("Etudiant {} affected to enseignant {} successfully.", idEtd, idEns);
    }

    @Override
    public List<Etudiant> findEtudiantsByEnseignant(EnseignantChercheur ens) {
        List<Etudiant> etudiants = etudiantRepository.findByEncadrant(ens);

        log.info("Find etudiant by enseignant successfully.");
        return etudiants;

    }
}

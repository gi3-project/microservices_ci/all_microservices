package tn.enis.memberservice;

import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import tn.enis.memberservice.beans.PublicationBean;
import tn.enis.memberservice.dao.EnseignantChercheurRepository;
import tn.enis.memberservice.dao.MemberRepository;
import tn.enis.memberservice.entity.EnseignantChercheur;
import tn.enis.memberservice.entity.Etudiant;
import tn.enis.memberservice.entity.Member;
import tn.enis.memberservice.proxies.PublicationProxyService;
import tn.enis.memberservice.service.IMemberService;

import java.util.Date;
import java.util.List;


@AllArgsConstructor
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class MemberServiceApplication{

    IMemberService memberService;

    MemberRepository memberRepository;
    EnseignantChercheurRepository enseignantChercheurRepository;
    PublicationProxyService publicationProxyService;
    public static void main(String[] args) {
        SpringApplication.run(MemberServiceApplication.class, args);
    }

//    @Override
//    public void run(String... args) throws Exception {
//        Etudiant etd1=Etudiant.builder()
//                .cin("123456")
//                .dateInscription(new Date())
//                .dateNaissance(new Date())
//                .diplome("mastère")
//                .email("etd1@gmail.com")
//                .password("pass1")
//                .encadrant(null)
//                .cv("cv1")
//                .nom("abid")
//                .prenom("youssef)")
//                .sujet("blockhain")
//                .build();
//        memberRepository.save(etd1);
//
//        EnseignantChercheur ens1=EnseignantChercheur.builder()
//                .cin("123458")
//                .grade("Professor")
//                .dateNaissance(new Date())
//                .etablissement("ENIS")
//                .email("ech@gmail.com")
//                .password("pass3")
//                .cv("cv3")
//                .nom("elleuch")
//                .prenom("haifa")
//                .build();
//        memberRepository.save(ens1);
//
//        List<Member> mbrs=memberRepository.findAll();
//        for(Member membre : mbrs){
//            System.out.println(membre.getNom()+" ");
//        }
//        for (Member membre : mbrs){
//            System.out.println(membre.getCin()+" "+membre.getEmail());
//        }
//        //chercher un membre par son Id
//        Member m1= memberRepository.findById(2L).get();
//        System.out.println(m1.getNom());
//        //modifier un membre
//        m1.setEmail("oussama.tayaro@enis.tn");
//        memberRepository.save(m1);
//        Member m2 = memberRepository.findByNomAndPrenom("tayaro", "goofy");
//        if (m2 != null) {
//            System.out.println(m2.getCin());
//        } else {
//            System.out.println("Aucun membre trouvé pour les critères de recherche.");
//        }
//        System.out.println(m2.getCin());
//        mbrs= memberRepository.findByNomStartingWith("t");
//        for(Member membre : mbrs){
//            System.out.println(membre.getDateNaissance());
//        }
//        List<EnseignantChercheur>Ech=enseignantChercheurRepository.findByGrade("Professor");
//        for(EnseignantChercheur enscher : Ech){
//            System.out.println(enscher.getCin());
//        }
//        //memberRepository.deleteById(1L);
//        Member membre= memberService.findMember(1L);
//        membre.setCv("cvvvv.pdf");
//        memberService.updateMember(membre);
//
//        memberService.affecterEtudiantToEnsignant(1L,3L);
//        memberService.affecterEtudiantToEnsignant(2L,3L);
//        Long idEnseignant=3L;
//        EnseignantChercheur enseignant = enseignantChercheurRepository.findById(idEnseignant).orElse(null);
//        String prenomEnseignant = (enseignant != null) ? enseignant.getPrenom() : "Enseignant introuvable";
//        List<Etudiant> etudiantsEncadres = memberService.findEtudiantsByEnseignant(enseignant);
//
//
//        System.out.println("Étudiants encadrés par l'enseignant :"+prenomEnseignant);
//        for (Etudiant etudiant : etudiantsEncadres) {
//            System.out.println(etudiant.getNom() + " " + etudiant.getPrenom());
//        }
//        PublicationBean publication = publicationProxyService.findPublicationById(1L);
//        System.out.println(publication.getTitre()+"  "+publication.getLien()+" "+publication.getLien()+" "+publication.getType());
//        //affecter des pubs a des auteurs
//        memberService.affecterauteurTopublication(1L,1L);
//        memberService.affecterauteurTopublication(2L,2L);
//        memberService.affecterauteurTopublication(3L,3L);
 //   }
}

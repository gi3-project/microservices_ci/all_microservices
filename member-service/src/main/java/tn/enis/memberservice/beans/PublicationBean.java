package tn.enis.memberservice.beans;

import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

import java.util.Date;

@Data
public class PublicationBean {
    private Long id;
    private String type;
    private String titre;
    private String lien;
    private Date date;
    private String sourcePdf;
}

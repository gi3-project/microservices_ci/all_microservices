package tn.enis.outilservice.service;


import tn.enis.outilservice.entity.Outil;

import java.util.List;

public interface IOutilService {
    //Crud sur les outils
    Outil addOutil(Outil o);

    void deleteOutil(Long id);

    Outil updateOutil(Outil o);

    Outil findOutil(Long id);

    List<Outil> findAll();
}

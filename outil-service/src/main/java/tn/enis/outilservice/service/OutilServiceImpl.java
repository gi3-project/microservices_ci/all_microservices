package tn.enis.outilservice.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tn.enis.outilservice.dao.OutilRepository;
import tn.enis.outilservice.entity.Outil;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class OutilServiceImpl implements IOutilService {

    OutilRepository outilRepository;

    @Override
    public Outil addOutil(Outil outil) {

        outilRepository.save(outil);

        log.info("Outil added successfully.");
        return outil;
    }

    @Override
    public void deleteOutil(Long id) {
        outilRepository.deleteById(id);
        log.info("Outil deleted successfully.");
    }

    @Override
    public Outil updateOutil(Outil o)
    {
        Outil outil = outilRepository.saveAndFlush(o);
        log.info("Outil updated successfully.");

        return outil;
    }

    @Override
    public Outil findOutil(Long id) {
        Outil outil = outilRepository.findById(id).get();

        log.info("Outil founded by id={} successfully.",id);
        return outil;
    }

    @Override
    public List<Outil> findAll() {
        List<Outil> outils = outilRepository.findAll();

        log.info("Find all outils successfully.");
        return outils;
    }

}

package tn.enis.outilservice;

import lombok.AllArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;



@AllArgsConstructor
@SpringBootApplication
@EnableDiscoveryClient
public class OutilServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(OutilServiceApplication.class, args);
    }
}

package tn.enis.outilservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.enis.outilservice.entity.Outil;


public interface OutilRepository extends JpaRepository<Outil,Long> {}

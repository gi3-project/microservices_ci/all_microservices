package tn.enis.outilservice.controller;

import java.util.List;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.outilservice.entity.Outil;
import tn.enis.outilservice.service.IOutilService;

@AllArgsConstructor
@RestController
@RequestMapping("/outils")
public class OutilController {
    IOutilService outilService;

    @GetMapping
    public List<Outil> findOutils() {
        return outilService.findAll();
    }

    @GetMapping("/{id}")
    public Outil findOneOutilById(@PathVariable Long id) {
        return outilService.findOutil(id);
    }

    @PostMapping
    public Outil addOutil(@RequestBody Outil outil) {
        return outilService.addOutil(outil);
    }

    @DeleteMapping("/{id}")
    public void deleteOutil(@PathVariable Long id) {
        outilService.deleteOutil(id);
    }

    @PutMapping("/{id}")
    public Outil updateOutil(@PathVariable Long id, @RequestBody Outil outil) {
        outil.setId(id);
        return outilService.updateOutil(outil);
    }
}

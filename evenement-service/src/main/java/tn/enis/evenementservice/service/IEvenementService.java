package tn.enis.evenementservice.service;


import tn.enis.evenementservice.entity.Evenement;

import java.util.List;

public interface IEvenementService {
    //Crud sur les evenements
    Evenement addEvenement(Evenement m);

    void deleteEvenement(Long id);

    Evenement updateEvenement(Evenement m);

    Evenement findEvenement(Long id);

    List<Evenement> findAll();
}

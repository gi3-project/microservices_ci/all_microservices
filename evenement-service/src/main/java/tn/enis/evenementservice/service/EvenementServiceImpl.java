package tn.enis.evenementservice.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tn.enis.evenementservice.dao.EvenementRepository;
import tn.enis.evenementservice.entity.Evenement;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class EvenementServiceImpl implements IEvenementService {

    EvenementRepository evenementRepository;

    @Override
    public Evenement addEvenement(Evenement evenement) {

        evenementRepository.save(evenement);

        log.info("Evenement added successfully.");
        return evenement;
    }

    @Override
    public void deleteEvenement(Long id) {
        evenementRepository.deleteById(id);
        log.info("Evenement deleted successfully.");
    }

    @Override
    public Evenement updateEvenement(Evenement o)
    {
        Evenement evenement = evenementRepository.saveAndFlush(o);
        log.info("Evenement updated successfully.");

        return evenement;
    }

    @Override
    public Evenement findEvenement(Long id) {
        Evenement evenement = evenementRepository.findById(id).get();

        log.info("Evenement founded by id={} successfully.",id);
        return evenement;
    }

    @Override
    public List<Evenement> findAll() {
        List<Evenement> evenements = evenementRepository.findAll();

        log.info("Find all evenements successfully.");
        return evenements;
    }

}

package tn.enis.evenementservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.enis.evenementservice.entity.Evenement;


public interface EvenementRepository extends JpaRepository<Evenement,Long> {}

package tn.enis.evenementservice.controller;

import java.util.List;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.evenementservice.entity.Evenement;
import tn.enis.evenementservice.service.IEvenementService;

@AllArgsConstructor
@RestController
@RequestMapping("/evenements")
public class EvenementController {
    IEvenementService eveneService;

    @GetMapping
    public List<Evenement> findEvenements() {
        return eveneService.findAll();
    }

    @GetMapping("/{id}")
    public Evenement findOneEvenementById(@PathVariable Long id) {
        return eveneService.findEvenement(id);
    }

    @PostMapping
    public Evenement addEvenement(@RequestBody Evenement evenement) {
        return eveneService.addEvenement(evenement);
    }

    @DeleteMapping("/{id}")
    public void deleteEvenement(@PathVariable Long id) {
        eveneService.deleteEvenement(id);
    }

    @PutMapping("/{id}")
    public Evenement updateEvenement(@PathVariable Long id, @RequestBody Evenement evenement) {
        evenement.setId(id);
        return eveneService.updateEvenement(evenement);
    }
}
